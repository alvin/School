  <!-- Main Sidebar Container -->
  <aside class="main-sidebar bg-light levation-4">
      <!-- Brand Logo -->
      <a href="./" class="brand-link text-center">
          <span class="brand-text font-weight-light">Zoyo School</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
          <!-- Sidebar Menu -->
          <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fab fa-ioxhost"></i>
                          <p>
                              Front Office
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="admissionenquiry.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Admission Enquiry</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="visitorbook.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Visitor Book</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="phonecalllog.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Phone Call Log</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="postaldispatch.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Postal Dispatch</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="postalreceive.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Postal Receive</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="complain.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Complain</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="setupfrontoffice.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Setup Front Office</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-user-plus"></i>
                          <p>
                              Student Information
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="studentsdetail.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Student Details</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="studentadmission.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Student Admission</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="onlineadmission.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Online Admission</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="disabledstudent.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Disabled Students</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="multiclassstudent.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Multi Class Student</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="bulkdeletestudent.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Bulk Delete</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="studentcategories.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Student Categories</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="studenthouse.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Student House</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="disablereason.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Disable Reason</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-money-bill"></i>
                          <p>
                              Fees Collection
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="collectfees.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Collect Fees</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="searchfeespayment.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Search Fees Payment</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="searchduesfees.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Search Due Fees</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="feesmaster.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Fees Master</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="feesgroup.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Fees Group</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="feestype.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Fees Type</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="feesdiscount.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Fees Discount</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="feescarryforward.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Fees Carry Forward</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="feesreminder.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Fees Reminder</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-dollar-sign"></i>
                          <p>
                              Income
                              <i class="right fas fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="addincome.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Add Income</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="searchincome.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Search Income</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="incomehead.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Income Head</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-credit-card"></i>
                          <p>
                              Expenses
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="addexpense.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Add Expense</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="searchexpense.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Search Expense</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="expensehead.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Expense Head</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-calendar-check"></i>
                          <p>
                              Attendance
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="studentattendance.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Student Attendance</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="studentattendancebydate.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Attendance By Date</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="studentapproveleave.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Approve Leave</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-graduation-cap"></i>
                          <p>
                              Academics
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Exam Group</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Exam Schedule</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Exam Result</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/read-mail.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Design Admit Card</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/read-mail.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Print Admit Card</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/read-mail.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Print Marksheet</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/read-mail.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Marks Grade</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-map"></i>
                          <p>
                              Examinations
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="examgroupadd.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Exam Group</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="examscheduleadd.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Exam Schedule</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="examresult.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Exam Result</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="designadmitcard.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Design Admit Card</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="printadmitcard.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Print Admit Card</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="designmarksheet.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Design Marksheet</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="printmarksheet.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Print Marksheet</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="marksgrade.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Marks Grade</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-rss"></i>
                          <p>
                              Online Examination
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="onlineexam.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Online Exam</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="questionsbank.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Question Bank</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-list-alt"></i>
                          <p>
                              Lesson Plan
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="managelessonplan.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Manage Lesson Plan</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="managesyllabusstatus.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Manage Syllabus Status</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="lesson.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Lesson</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="topic.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Topic</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-th-list"></i>
                          <p>
                              Regulation
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="classtimetable.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Class Timetable</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Teachers Timetable</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="assignclassteacher.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Assign Class Teacher</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="promotestudents.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Promote Students</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="subjectgroup.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Subject Group</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="subjects.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Subjects</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="class.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Class</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="sections.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Sections</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-sitemap"></i>
                          <p>
                              Human Resource
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="staffdictionary.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Staff Dictionary</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="staffattendance.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Staff Attendance</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="payroll.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Payroll</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="approveleaverequest.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Approve Leave Request</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="applyleavestaff.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Apply Leave</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="addleavetype.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Leave Type</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="teachersrating.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Teachers Rating</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="department.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Department</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="designation.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Designation</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="disabledstafflist.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Disabled Staff</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-flask"></i>
                          <p>
                              Homework
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="homeworkadd.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Add Homework</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-book"></i>
                          <p>
                              Library
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="booklist.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Book List</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="issuereturnbook.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Issue Return</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="addstudentbook.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Add Student</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="addstaffmember.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Add Staff Member</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-object-group"></i>
                          <p>
                              Inventory
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Issue Item</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Add Item Stock</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Add Item</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Item Category</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Item Store</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Item Supplier</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-bus"></i>
                          <p>
                              Transport
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="routes.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Routes</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="vehicles.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Vehicles</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="assignvehicle.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Assign Vehicles</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-building"></i>
                          <p>
                              Hostel
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="hostelrooms.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Hostel Rooms</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="roomtype.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Room Type</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="hostel.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Hostel</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-newspaper"></i>
                          <p>
                              Certificate
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="studentcertificate.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Student Certificate</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="generatecertificate.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Generate Certificate</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="studentidcard.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Student ID Card</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="generateidcard.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Generate ID Card</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="staffidcard.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Staff ID Card</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="generatestaffidcard.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Generate Staff ID Card</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fab fa-empire"></i>
                          <p>
                              Front CMS
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Event</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Gallery</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>News</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Media Manager</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Pages</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Menus</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Banner Images</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-universal-access"></i>
                          <p>
                              Alumni
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="managealumni.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Manage Alumni</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="events.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Events</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-chart-line"></i>
                          <p>
                              Reports
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="studentinformation.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Student Information</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="finance.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Finance</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Attendance</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="rankreport.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Examinations</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Online Examinations</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="lessonplan.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Lesson Plan</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="humanresource.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Human Resource</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="library.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Library</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Inventory</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="transport.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Transport</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Hostel</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Alumni</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="userlog.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>User Log</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Audit Trail Report</p>
                              </a>
                          </li>
                      </ul>
                  </li>

                  <li class="nav-item">
                      <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-cogs"></i>
                          <p>
                              System Setting
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="generalsetting.php" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>General Setting</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Session Setting</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Notification Setting</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>SMS Setting</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Email Setting</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Payment Methods</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Print Header Footer</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Front CMS Setting</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Roles Permissions</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Backup/Restore</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Languages</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Users</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Modules</p>
                              </a>
                          </li>
                          <li class="nav-item">
                              <a href="pages/mailbox/mailbox.html" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Custom Fields</p>
                              </a>
                          </li>
                      </ul>
                  </li>
              </ul>
          </nav>
          <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
  </aside>