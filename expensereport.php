<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Expense Report - Zoyo School</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
    <!-- calender css -->
    <link rel="stylesheet" href="calendar/dist/style.css">
    <link rel="stylesheet" href="dist/css/style.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- Preloader -->

        <!-- top navbar -->
        <?php include('topnav.php') ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?php include('sidebar.php') ?>
        <!-- main sidebar end -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">

            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="studentinformation">
                                <!-- studentinfolinks contains all links -->
                                <!-- this is a .php file -->
                                <?php include('financelinks.php') ?>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-2">
                                    <div class="border py-1 px-3 mb-1">
                                        <h4>Select Criteria</h4>
                                    </div>
                                    <div class="border p-3">
                                        <form action="">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="searchtype">Search Type</label>
                                                        <select class="form-control" id="searchtype">
                                                            <option selected value="">Select</option>
                                                            <option>Today</option>
                                                            <option>This Week</option>
                                                            <option>Last Week</option>
                                                            <option>This Month</option>
                                                            <option>Last Month</option>
                                                            <option>Last 3 Months</option>
                                                            <option>Last 6 Months</option>
                                                            <option>Last 12 Months</option>
                                                            <option>This Year</option>
                                                            <option>Last Year</option>
                                                            <option>Period</option>
                                                        </select>
                                                        <span class=""></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row text-right">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-secondary">Search</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- list -->
                            <div class="row mt-4">
                                <div class="col-lg-12">
                                    <div class="row mb-2">
                                        <div class="col-md-12">
                                            <div class="border px-3 py-1">
                                                <h4>Expense Report</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="row justify-content-between">
                                            <div class="col-md-4 mb-2">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search" title="Type in a name">
                                                </div>
                                            </div>
                                            <div class="col-md-3 text-center mb-2">
                                                <button type="submit" class="btn border"><i class="fas fa-copy"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-file-excel"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-file-csv"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-file-pdf"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-print"></i></button>
                                            </div>
                                        </div>
                                        <!-- table -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <form action="#">
                                                        <table id="myTable" class="table border table-hover">
                                                            <tr class="header">
                                                                <th scope="col">Expense ID</th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col">Expense Head</th>
                                                                <th scope="col">Name</th>
                                                                <th scope="col">Invoice Number</th>
                                                                <th scope="col">Amount(&dollar;)</th>
                                                                <th scope="col" class="text-center">Action</th>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <div class="" style="width: 120px;">
                                                                        <div class="row">
                                                                            <div class="col-md-4 col-4">
                                                                                <form action="#">
                                                                                    <button type="submit" class="btn border mb-1 mr-1"><span><i class="fas fa-file-pdf"></i></span></button>
                                                                                </form>
                                                                            </div>
                                                                            <div class="col-md-4 col-4">
                                                                                <form action="#">
                                                                                    <button type="submit" class="btn border mb-1 mr-1"><span><i class="fas fa-edit"></i></span></button>
                                                                                </form>
                                                                            </div>
                                                                            <div class="col-md-4 col-4">
                                                                                <form action="#">
                                                                                    <button type="submit" class="btn border mb-1"><span><i class="fas fa-times"></i></span></button>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- table end -->
                                    </div>
                                </div>
                                <!-- ./col -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2021 <a href="https://zoyoecommerce.com">Zoyo E-commerce Pvt. Ltd.</a></strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b class="mr-1">Version</b>0.1
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <script type="text/javascript">
        $(document).ready(function() {
            $(studentInfo).click(function() {
                $(demo).show();

            });
        });
    </script>
    }

    <!-- table search js -->
    <script src="/dist/js/tablescript.js"></script>
    <!-- end table search js -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="plugins/moment/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
</body>

</html>