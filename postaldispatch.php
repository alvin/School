<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Postal Dispatch - Zoyo School</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

    <!-- calender css -->
    <link rel="stylesheet" href="calendar/dist/style.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- Preloader -->

        <!-- top navbar -->
        <?php include('topnav.php') ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?php include('sidebar.php') ?>
        <!-- main sidebar end -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">

            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <!-- Small boxes (Stat box) -->

                    <div class="card">
                        <div class="card-body">
                            <!-- select criteria -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>Add Postal Dispatch</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="">
                                                <form action="#">
                                                    <form>
                                                        <div class="row">
                                                            <div class="col-md-3 mb-1">
                                                                <div class="form-group">
                                                                    <label for="totitle">To Title</label>
                                                                    <input type="text" class="form-control" id="totitle">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                                <div class="form-group">
                                                                    <label for="name">Reference No</label>
                                                                    <input type="text" class="form-control" id="name">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                                <div class="form-group">
                                                                    <label for="address">Address</label>
                                                                    <textarea name="address" id="address" class="form-control"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                                <div class="form-group">
                                                                    <label for="note">Note</label>
                                                                    <textarea name="note" id="note" class="form-control"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3 mb-1">
                                                                <div class="form-group">
                                                                    <label for="fromtitle">From Title</label>
                                                                    <input type="text" class="form-control" id="fromtitle">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                                <div class="form-group">
                                                                    <label for="date">Date</label>
                                                                    <input type="date" class="form-control" id="date">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 mb-1">
                                                                <div class="form-group">
                                                                    <label for="attachdoc">Attach Document</label>
                                                                    <input type="file" name="attachdoc" class="form-control" id="attachdoc">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row justify-content-end text-center">
                                                            <div class="col-md-2 text-right"><button type="submit" class="btn btn-secondary"><span>Save</span></button></div>
                                                        </div>
                                                    </form>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ./col -->
                            </div>
                            <!-- /.row -->

                            <!-- admissin enquiry -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="row justify-content-between">
                                                <div class="col-md-3">
                                                    <h4>Postal Dispatch List</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="">
                                                <div class="row justify-content-between">
                                                    <div class="col-md-4 mb-2">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search" title="Type in a name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 text-center mb-2">
                                                        <button type="submit" class="btn border"><i class="fas fa-copy"></i></button>
                                                        <button type="submit" class="btn border"><i class="fas fa-file-excel"></i></button>
                                                        <button type="submit" class="btn border"><i class="fas fa-file-csv"></i></button>
                                                        <button type="submit" class="btn border"><i class="fas fa-file-pdf"></i></button>
                                                        <button type="submit" class="btn border"><i class="fas fa-print"></i></button>
                                                    </div>
                                                </div>
                                                <!-- table -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="table-responsive">
                                                            <form action="#">
                                                                <table id="myTable" class="table border table-hover">
                                                                    <tr class="header">
                                                                        <th scope="col">To Title</th>
                                                                        <th scope="col">Reference No</th>
                                                                        <th scope="col">From Title</th>
                                                                        <th scope="col">Date</th>
                                                                        <th scope="col" class="text-center">Action</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Health</td>
                                                                        <td>34567890</td>
                                                                        <td>Head Office</td>
                                                                        <td>20/03/2021</td>
                                                                        <td>
                                                                            <div class="" style="width: 120px;">
                                                                                <div class="row">
                                                                                    <div class="col-md-4 col-4">
                                                                                        <form action="#">
                                                                                            <button type="submit" class="btn border mb-1 mr-1"><span><i class="fas fa-phone-alt"></i></span></button>
                                                                                        </form>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-4">
                                                                                        <form action="#">
                                                                                            <button type="submit" class="btn border mb-1 mr-1"><span><i class="fas fa-pen"></i></span></button>
                                                                                        </form>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-4">
                                                                                        <form action="#">
                                                                                            <button type="submit" class="btn border mb-1"><span><i class="fas fa-times"></i></span></button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Health</td>
                                                                        <td>34567890</td>
                                                                        <td>Head Office</td>
                                                                        <td>20/03/2021</td>
                                                                        <td>
                                                                            <div class="" style="width: 120px;">
                                                                                <div class="row">
                                                                                    <div class="col-md-4 col-4">
                                                                                        <form action="#">
                                                                                            <button type="submit" class="btn border mb-1 mr-1"><span><i class="fas fa-phone-alt"></i></span></button>
                                                                                        </form>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-4">
                                                                                        <form action="#">
                                                                                            <button type="submit" class="btn border mb-1 mr-1"><span><i class="fas fa-pen"></i></span></button>
                                                                                        </form>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-4">
                                                                                        <form action="#">
                                                                                            <button type="submit" class="btn border mb-1"><span><i class="fas fa-times"></i></span></button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Health</td>
                                                                        <td>34567890</td>
                                                                        <td>Head Office</td>
                                                                        <td>20/03/2021</td>
                                                                        <td>
                                                                            <div class="" style="width: 120px;">
                                                                                <div class="row">
                                                                                    <div class="col-md-4 col-4">
                                                                                        <form action="#">
                                                                                            <button type="submit" class="btn border mb-1 mr-1"><span><i class="fas fa-phone-alt"></i></span></button>
                                                                                        </form>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-4">
                                                                                        <form action="#">
                                                                                            <button type="submit" class="btn border mb-1 mr-1"><span><i class="fas fa-pen"></i></span></button>
                                                                                        </form>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-4">
                                                                                        <form action="#">
                                                                                            <button type="submit" class="btn border mb-1"><span><i class="fas fa-times"></i></span></button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- table end -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- ./col -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
                <!-- container fluid end -->
            </section>
        </div>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2021 <a href="https://zoyoecommerce.com">Zoyo E-commerce Pvt. Ltd.</a></strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b class="mr-1">Version</b>0.1
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- table search js -->
    <script>
        function myFunction() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
    <!-- end table search js -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="plugins/moment/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
</body>

</html>