<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Students Detail - Zoyo School</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

    <!-- calender css -->
    <link rel="stylesheet" href="calendar/dist/style.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- Preloader -->

        <!-- top navbar -->
        <?php include('topnav.php') ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?php include('sidebar.php') ?>
        <!-- main sidebar end -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header"></div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="">
                                <form action="">
                                    <!-- select criteria -->

                                    <div class="p-2 border">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5>Student Admission</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 mb-1">
                                                <label for="admissionno">Admission No</label>
                                                <input type="number" class="form-control" id="admissionno">
                                            </div>
                                            <div class="col-md-3 mb-1">
                                                <label for="rollno">Roll No</label>
                                                <input type="text" class="form-control" id="rollno">
                                            </div>
                                            <div class="col-md-3 mb-1">
                                                <div class="form-group">
                                                    <label for="ControlSelect1">Class</label>
                                                    <select class="form-control" id="ControlSelect1">
                                                        <option selected>Select</option>
                                                        <option>Class 1</option>
                                                        <option>Class 2</option>
                                                        <option>Class 3</option>
                                                        <option>Class 4</option>
                                                        <option>Class 5</option>
                                                        <option>Class 6</option>
                                                        <option>Class 7</option>
                                                        <option>Class 8</option>
                                                        <option>Class 9</option>
                                                        <option>Class 10</option>
                                                        <option>Class 11</option>
                                                        <option>Class 12</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3 mb-1">
                                                <div class="form-group">
                                                    <label for="section">Section</label>
                                                    <select class="form-control" id="section">
                                                        <option selected>Select</option>
                                                        <option>Section A</option>
                                                        <option>Section B</option>
                                                        <option>Section C</option>
                                                        <option>Section D</option>
                                                        <option>Section E</option>
                                                        <option>Section F</option>
                                                        <option>Section G</option>
                                                        <option>Section H</option>
                                                        <option>Section I</option>
                                                        <option>Section J</option>
                                                        <option>Section K</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="firstname">First Name</label>
                                                    <input type="text" class="form-control" id="firstname">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="lastname">Last Name</label>
                                                    <input type="text" class="form-control" id="lastname">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="gender">Gender</label>
                                                    <select class="form-control" id="gender">
                                                        <option selected>Select</option>
                                                        <option>Male</option>
                                                        <option>Female</option>
                                                        <option>Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="dob">Date of Birth</label>
                                                    <input type="date" class="form-control" id="dob">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="category">Category</label>
                                                    <select class="form-control" id="category">
                                                        <option selected>Select</option>
                                                        <option>General</option>
                                                        <option>OBC</option>
                                                        <option>Special</option>
                                                        <option>Physically Challenged</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="religion">Religion</label>
                                                    <input type="text" class="form-control" id="religion">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="caste">Caste</label>
                                                    <input type="text" class="form-control" id="caste">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="mobile">Mobile No</label>
                                                    <input type="number" class="form-control" id="mobile">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="email" class="form-control" id="email">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="admissiondate">Admission Date</label>
                                                    <input type="date" class="form-control" id="admissiondate">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="studentphoto">Student Photo</label>
                                                    <input type="file" class="form-control" id="studentphoto">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="bloodgroup">Blood Group</label>
                                                    <select class="form-control" id="bloodgroup">
                                                        <option selected>Select</option>
                                                        <option>O+</option>
                                                        <option>A+</option>
                                                        <option>B+</option>
                                                        <option>O-</option>
                                                        <option>A-</option>
                                                        <option>B-</option>
                                                        <option>AB-</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="height">Height</label>
                                                    <input type="text" class="form-control" id="height">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="weight">Weight</label>
                                                    <input type="text" class="form-control" id="weight">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="asondate">As on Date</label>
                                                    <input type="date" class="form-control" id="asondate">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="studenthouse">Student House</label>
                                                    <select class="form-control" id="studenthouse">
                                                        <option selected>Select</option>
                                                        <option>White</option>
                                                        <option>Green</option>
                                                        <option>Blue</option>
                                                        <option>Yellow</option>
                                                        <option>Pink</option>
                                                        <option>Red</option>
                                                        <option>Black</option>
                                                        <option>Orange</option>
                                                        <option>Black</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="medicalhistory">Medical History</label>
                                                    <textarea name="medicalhistory" id="medicalhistory" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="my-4">

                                        <!-- parent gurdian detail -->
                                        <div class="">
                                            <div class="row py-2">
                                                <div class="col-md-12">
                                                    <h5>Parent Guardian Detail</h5>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3 mb-1">
                                                    <label for="fathername">Father Name</label>
                                                    <input type="text" class="form-control" id="fathername">
                                                </div>
                                                <div class="col-md-3 mb-1">
                                                    <label for="fatherphone">Father Phone No</label>
                                                    <input type="text" class="form-control" id="rollno">
                                                </div>
                                                <div class="col-md-3 mb-1">
                                                    <div class="form-group">
                                                        <label for="fatheroccupation">Father Occupation</label>
                                                        <input type="text" class="form-control" id="fatheroccupation">
                                                    </div>
                                                </div>

                                                <div class="col-md-3 mb-1">
                                                    <div class="form-group">
                                                        <label for="fatherphoto">Father Photo</label>
                                                        <input type="file" class="form-control" id="fatherphoto">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3 mb-1">
                                                    <label for="mothername">Mother Name</label>
                                                    <input type="text" class="form-control" id="mothername">
                                                </div>
                                                <div class="col-md-3 mb-1">
                                                    <label for="motherphone">Mother Phone No</label>
                                                    <input type="number" class="form-control" id="motherphone">
                                                </div>
                                                <div class="col-md-3 mb-1">
                                                    <div class="form-group">
                                                        <label for="motheroccupation">Mother Occupation</label>
                                                        <input type="text" class="form-control" id="motheroccupation">
                                                    </div>
                                                </div>

                                                <div class="col-md-3 mb-1">
                                                    <div class="form-group">
                                                        <label for="motherphoto">Mother Photo</label>
                                                        <input type="file" class="form-control" id="motherphoto">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="ifguardianis">If Guardian Is</label>
                                                        <select class="form-control" id="ifguardianis">
                                                            <option selected>Select</option>
                                                            <option>Father</option>
                                                            <option>Mother</option>
                                                            <option>Other</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3 mb-1">
                                                    <label for="guardianname">Guardian Name</label>
                                                    <input type="text" class="form-control" id="guardianname">
                                                </div>

                                                <div class="col-md-3 mb-1">
                                                    <label for="guardianrelation">Guardian Relation</label>
                                                    <input type="text" class="form-control" id="guardianrelation">
                                                </div>

                                                <div class="col-md-3 mb-1">
                                                    <label for="guardianemail">Guardian Email</label>
                                                    <input type="email" class="form-control" id="guardianemail">
                                                </div>

                                                <div class="col-md-3 mb-1">
                                                    <div class="form-group">
                                                        <label for="guardianphoto">Guardian Photo</label>
                                                        <input type="file" class="form-control" id="guardianphoto">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3 mb-1">
                                                    <label for="guardianphone">Guardian Phone No</label>
                                                    <input type="number" class="form-control" id="guardianphone">
                                                </div>
                                                <div class="col-md-3 mb-1">
                                                    <div class="form-group">
                                                        <label for="guardianoccupation">Guardian Occupation</label>
                                                        <input type="text" class="form-control" id="guardianoccupation">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-1">
                                                    <div class="form-group">
                                                        <label for="guardianaddress">Guardian Address</label>
                                                        <textarea name="guardianaddress" id="guardianaddress" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="card">
                                                        <a class="" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                            <div class="card-header text-dark">
                                                                <h5> Add More Detail</h5>
                                                            </div>
                                                        </a>

                                                        <div class="collapse" id="collapseExample">
                                                            <div class="card-body">
                                                                <div class="studentaddressdetail mb-4">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="card-title">
                                                                                <h5>Student Address Details</h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6 mb-1">
                                                                            <label for="currentaddress">Current Address</label>
                                                                            <textarea name="currentaddress" id="currentaddress" class="form-control"></textarea>
                                                                        </div>
                                                                        <div class="col-md-6 mb-1">
                                                                            <label for="permanentaddress">Permanent Address</label>
                                                                            <textarea name="permanentaddress" id="permanentaddress" class="form-control"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="transportdetail mb-4">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="card-title">
                                                                                <h5>Transport Details</h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="routelist">Route List</label>
                                                                                <select class="form-control" id="routelist">
                                                                                    <option selected>Select</option>
                                                                                    <option>Bijnor-Chandpur</option>
                                                                                    <option>Bijnor-Dhampur</option>
                                                                                    <option>Bijnor-Najibabad</option>
                                                                                    <option>Bijnor-Moradabad</option>
                                                                                    <option>Bijnor-Muzzafarnagar</option>
                                                                                    <option>Bijnor-Meerut</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6"></div>
                                                                    </div>
                                                                </div>

                                                                <div class="transportdetail">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="card-title">
                                                                                <h5>Hostel Details</h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="routelist">Hostel</label>
                                                                                <select class="form-control" id="routelist">
                                                                                    <option selected>Select</option>
                                                                                    <option>Boys-Hostel 01</option>
                                                                                    <option>Boys-Hostel 02</option>
                                                                                    <option>Boys-Hostel 03</option>
                                                                                    <option>Girls-Hostel 01</option>
                                                                                    <option>Girls-Hostel 02</option>
                                                                                    <option>Girls-Hostel 03</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="routelist">Room No</label>
                                                                                <select class="form-control" id="routelist">
                                                                                    <option selected>Select</option>
                                                                                    <option>01</option>
                                                                                    <option>02</option>
                                                                                    <option>03</option>
                                                                                    <option>04</option>
                                                                                    <option>05</option>
                                                                                    <option>06</option>
                                                                                    <option>07</option>
                                                                                    <option>08</option>
                                                                                    <option>09</option>
                                                                                    <option>10</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="miscellaneousdetails ">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="card-title">
                                                                                <h5>Miscellaneous Details </h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-4 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="bankaccno">
                                                                                    Bank Account Number
                                                                                </label>
                                                                                <input type="text" class="form-control" id="bankaccno">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="bankname">Bank Name</label>
                                                                                <input type="text" class="form-control" id="bankname">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="ifsccode">IFSC Code</label>
                                                                                <input type="text" class="form-control" id="ifsccode">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-4 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="nationalidenno">National Identification Number</label>
                                                                                <input type="text" class="form-control" id="nationalidenno">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="localidenno">Local Identification Number</label>
                                                                                <input type="text" class="form-control" id="localidenno">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="ifsccode">RTE</label>
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="radio" name="exampleRadios" id="ifsccode" value="option1" checked>
                                                                                    <label class="form-check-label mr-4" for="ifsccode">
                                                                                        Yes
                                                                                    </label>
                                                                                    <input class="form-check-input" type="radio" name="exampleRadios" id="ifsccode" value="option2">
                                                                                    <label class="form-check-label" for="ifsccode">
                                                                                        No
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="previousschooldetail">Previous School Details</label>
                                                                                <textarea name="previousschooldetail" id="previousschooldetail" class="form-control"></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="note">Note</label>
                                                                                <textarea name="note" id="note" class="form-control"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="transportdetail">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="card-title">
                                                                                <h5>Upload Documents</h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-3 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="title1">Title</label>
                                                                                <input type="text" class="form-control" id="title1">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="file2">Documents</label>
                                                                                <input type="file" class="form-control" id="file1">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="title2">Title</label>
                                                                                <input type="text" class="form-control" id="title2">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="file2">Documents</label>
                                                                                <input type="file   " class="form-control" id="file2">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- /.row -->
                                    <div class="row justify-content-end mt-2">
                                        <div class="col-md-4 text-right">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-secondary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- container fluid end -->
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2021 <a href="https://zoyoecommerce.com">Zoyo E-commerce Pvt. Ltd.</a></strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b class="mr-1">Version</b>0.1
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- table search js -->
    <script>
        function myFunction() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
    <!-- end table search js -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="plugins/moment/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
</body>

</html>