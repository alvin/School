    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-md-block">
                <a href="./" class="nav-link navbar-brand"><strong>Zoyo School</strong></a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Navbar Search -->
            <li class="nav-item d-none d-lg-block"><span id="timer" class="nav-link"></span></li>
            <li class="nav-item d-none d-lg-block">
                <a class="nav-link" href="#"><i class="fas fa-shopping-cart mr-1"></i>Premium</a>
            </li>
            <!-- <li class="nav-item d-none d-lg-none">
                <a class="nav-link" href="#"><i class="fas fa-shopping-cart mr-1"></i></a>
            </li> -->
            <li class="nav-item d-none d-sm-inline-block">
                <div class="input-group">
                    <input type="text" class="form-control">
                    <a class="nav-link" type="button"><i class="fas fa-search float"></i></a>
                </div>

                <div class="navbar-search-block">
                    <form class="form-inline">
                        <div class="input-group input-group-sm">
                            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn btn-navbar" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </li>

            <li class="nav-item">
                <a href="#" class="nav-link"><i class="fas fa-calendar-alt"></i></a>
            </li>

            <li class="nav-item">
                <a href="#" class="nav-link"><i class="far fa-check-square"></i></a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link"><i class="fab fa-whatsapp"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                    <i class="fas fa-th"></i>
                </a>
            </li>
            <!--User Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-user-circle"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <a href="#" class="dropdown-item mt-2">
                        <img src="dist/img/avatar.png" class="img-fluid w-25" alt=""> <span>Zoyo School</span>
                    </a>
                    <div class="py-3">
                        <a href="#" class="dropdown-item text-center">
                            <a href="#" class="mx-2" data-bs-toggle="tooltip" data-bs-placement="top" title="My profile"><i class="fas fa-user-alt"></i><span>Profile</span></a>
                            <a href="#" class="mx-2" data-bs-toggle="tooltip" data-bs-placement="top" title="Change password"><i class="fas fa-user-lock"></i><span>Password</span></a>
                            <a href="#" class="mx-2" data-bs-toggle="tooltip" data-bs-placement="top" title="Logout Account"><i class="fas fa-sign-out-alt"></i><span>Logout</span></a>
                        </a>
                    </div>
                </div>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    
  <!-- timer countdown script -->
  <script>
    // Set the date we're counting down to
    var countDownDate = new Date("Nov 1, 2021 15:37:25").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get today's date and time
      var now = new Date().getTime();

      // Find the distance between now and the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Output the result in an element with id="demo"
      document.getElementById("timer").innerHTML = days + "d:" + hours + "h:" +
        minutes + "m:" + seconds + "s";

      // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("timer").innerHTML = "EXPIRED";
      }
    }, 1000);
  </script>
