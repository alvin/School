<div class="row">
    <div class="col-md-12 mb-2 student-info-links">
        <div class="border px-3 py-1 mb-1">
            <h4>Library Report</h4>
        </div>
        <div class="border p-3">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="bookissuereport.php">Book Issue Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="bookduereport.php">Book Due Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="bookinventoryreport.php">Book Inventory Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="bookissuereturnreport.php">Book Issue Return Report</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>