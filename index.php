<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Zoyo School</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

  <!-- calender css -->
  <link rel="stylesheet" href="calendar/dist/style.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <!-- Preloader -->

    <!-- top navbar -->
    <?php include('topnav.php') ?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?php include('sidebar.php') ?>
    <!-- main sidebar end -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-12">
              <h1 class="m-0">Dashboard</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner text-center">
                  <p><i class="fas fa-money-bill mr-1"></i>Fees Awaiting Payment<span class="ml-2">0/8</span></p>
                </div>
                <div class="progress">
                  <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 60%"></div>
                </div>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner text-center">
                  <p><span class="text-left"><i class="fab fa-ioxhost mr-3"></i>Converted Leads</span><span class="ml-3">0/8</span></p>
                </div>
                <div class="progress">
                  <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 60%"></div>
                </div>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner text-center">
                  <p><i class="fas fa-calendar-check mr-2"></i>Staff Present Today<span class="ml-3">0/8</span></p>
                </div>
                <div class="progress">
                  <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 60%"></div>
                </div>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner text-center">
                  <p><i class="fas fa-calendar-check mr-2"></i>Student Present Today<span class="ml-2">0/8</span></p>
                </div>
                <div class="progress">
                  <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 60%"></div>
                </div>
              </div>
            </div>
            <!-- ./col -->
          </div>
          <!-- /.row -->

          <!-- row -->
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header bg-info">
                  <h3 class="card-title">
                    Fees Collection & Expenses For 2021
                  </h3>
                </div><!-- end card-header -->
                <div class="card-body">
                  <div class="tab-content p-0">
                    <div><span>January</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 60%">60%</div>
                      </div>
                      <span>February</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 70%">70%</div>
                      </div>
                      <span>March</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%">50%</div>
                      </div>
                      <span>April</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 65%">65%</div>
                      </div>
                      <span>May</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 70%">70%</div>
                      </div>
                      <span>June</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 60%">60%</div>
                      </div>
                      <span>July</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 78%">78%</div>
                      </div>
                      <span>August</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 66%">66%</div>
                      </div>
                      <span>September</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 80%">80%</div>
                      </div>
                      <span>October</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 78%">78%</div>
                      </div>
                      <!-- <span>November</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">60%</div>
                      </div>
                      <span>December</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">60%</div>
                      </div> -->
                    </div>
                  </div>
                </div><!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- ./ left col -->

            <!-- right col -->
            <div class="col-md-6">
              <div class="card">
                <div class="card-header bg-success">
                  <h3 class="card-title">
                    Fees Collection & Expenses For 2021
                  </h3>
                </div><!-- end card-header -->
                <div class="card-body">
                  <div class="tab-content p-0">
                    <div><span>January</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 60%">60%</div>
                      </div>
                      <span>February</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 70%">70%</div>
                      </div>
                      <span>March</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%">50%</div>
                      </div>
                      <span>April</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 65%">65%</div>
                      </div>
                      <span>May</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 70%">70%</div>
                      </div>
                      <span>June</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 60%">60%</div>
                      </div>
                      <span>July</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 78%">78%</div>
                      </div>
                      <span>August</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 66%">66%</div>
                      </div>
                      <span>September</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 80%">80%</div>
                      </div>
                      <span>October</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 78%">78%</div>
                      </div>
                      <!-- <span>November</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">60%</div>
                      </div>
                      <span>December</span>
                      <div class="progress mb-1">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">60%</div>
                      </div> -->
                    </div>
                  </div>
                </div><!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- ./ right col -->
          </div>
          <!-- /.row -->

          <div class="row">
            <div class="col-lg-6">
              <div class="card">
                <div class="card-header bg-success border-0">
                  <h3 class="card-title">
                    Fees Overview
                  </h3>
                </div>
                <div class="card-body">
                  <span>October</span>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 78%">78%</div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card -->
            <div class="col-lg-6">
              <div class="card">
                <div class="card-header bg-info border-0">
                  <h3 class="card-title">
                    Expense - October 2021
                  </h3>
                </div>
                <div class="card-body">
                  <span>October</span>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 58%">58%</div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- end row -->

          <!-- overview row -->
          <!-- row -->
          <div class="row">
            <div class="col-md-3">
              <div class="card">
                <div class="card-header bg-danger border-0">
                  <h3 class="card-title">
                    Fees Overview
                  </h3>
                </div>
                <div class="card-body">
                  <span>8 UNPAID</span>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>
                  </div>

                  <span>8 PARTIAL</span>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                  </div>

                  <span>8 PAID</span>
                  <div class="progress mb-1">
                    <div class="progress-sm-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>

            <div class="col-md-3">
              <div class="card">
                <div class="card-header bg-info border-0">
                  <h3 class="card-title">
                    Enquiry Overview
                  </h3>
                </div>
                <div class="card-body">
                  <div class="row">
                    <span class="ml-2">0 Active</span><span class="ml-auto mr-2">0%</span>
                  </div>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                  </div>
                  <div class="row">
                    <span class="ml-2">0 Won</span><span class="ml-auto mr-2">0%</span>
                  </div>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                  </div>
                  <div class="row">
                    <span class="ml-2">0 Passive</span><span class="ml-auto mr-2">0%</span>
                  </div>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                  </div>
                  <div class="row">
                    <span class="ml-2">0 Lost</span><span class="ml-auto mr-2">0%</span>
                  </div>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                  </div>
                  <div class="row">
                    <span class="ml-2">0 Dead</span><span class="ml-auto mr-2">0%</span>
                  </div>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>

            <div class="col-md-3">
              <div class="card">
                <div class="card-header bg-success border-0">
                  <h3 class="card-title">
                    Library Overview
                  </h3>
                </div>
                <div class="card-body text-uppercase">
                  <span>0 Due For Return</span>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                  </div>
                  <span>0 Returned</span>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                  </div>
                  <div class="row">
                    <span class="ml-2">Issued out of</span><span class="ml-auto mr-2">0%</span>
                  </div>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                  </div>
                  <div class="row">
                    <span class="ml-2">0 Available out of</span><span class="ml-auto mr-2">0%</span>
                  </div>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>

            <div class="col-md-3">
              <div class="card">
                <div class="card-header bg-warning border-0">
                  <h3 class="card-title">
                    Student Today Attendance
                  </h3>
                </div>
                <div class="card-body text-uppercase">
                  <span></span>
                  <div class="row">
                    <span class="ml-2">10 Present</span><span class="ml-auto mr-2">57.12%</span>
                  </div>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 57.12%"></div>
                  </div>
                  <div class="row">
                    <span class="ml-2">1 Late</span><span class="ml-auto mr-2">4.76%</span>
                  </div>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 4.76%"></div>
                  </div>
                  <div class="row">
                    <span class="ml-2">1 Absent</span><span class="ml-auto mr-2">4.76%</span>
                  </div>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 4.76%"></div>
                  </div>
                  <div class="row">
                    <span class="ml-2">1 Half Day</span><span class="ml-auto mr-2">4.76%</span>
                  </div>
                  <span></span>
                  <div class="progress mb-1">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 4.76%"></div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /. overview row end -->

          <!-- monthly collection row start -->
          <div class="row">
            <div class="col-lg-4">
              <div class="card">
                <div class="card-header bg-info border-0">
                  <h3 class="card-title text-uppercase">
                    <i class="nav-icon fas fa-money-bill mr-2"></i>Monthly Fees Collection
                  </h3>
                </div>
                <div class="card-body text-uppercase">
                  <h4>&dollar;10000</h4>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <div class="col-lg-4">
              <div class="card">
                <div class="card-header bg-info border-0">
                  <h3 class="card-title text-uppercase">
                    <i class="nav-icon fas fa-credit-card mr-2"></i>Monthly Expenses
                  </h3>
                </div>
                <div class="card-body text-uppercase">
                  <h4>&dollar;3000</h4>
                </div>
              </div>
              <!-- /.card -->
            </div>
            <div class="col-lg-4">
              <div class="card">
                <div class="card-header bg-info border-0">
                  <h3 class="card-title text-uppercase">
                    <i class="nav-icon fas fa-user mr-2"></i> Student
                  </h3>
                </div>
                <div class="card-body text-uppercase">
                  <h4>25</h4>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /. row end -->

          <div class="row">
            <div class="col-md-4">
              <div class="card">
                <div class="card-header bg-info border-0">
                  <h4 class="card-title text-uppercase">
                    <i class="fas fa-user-secret mr-2"></i>Admin
                  </h4>
                </div>
                <div class="card-body text-uppercase text-center">
                  <h4>1</h4>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card">
                <div class="card-header bg-info border-0">
                  <h4 class="card-title text-uppercase">
                    <i class="fas fa-user-secret mr-2"></i>Teacher
                  </h4>
                </div>
                <div class="card-body text-uppercase text-center">
                  <h4>2</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <div class="card-header bg-info border-0">
                  <h4 class="card-title text-uppercase">
                    <i class="fas fa-user-secret mr-2"></i>Accountant
                  </h4>
                </div>
                <div class="card-body text-uppercase text-center">
                  <h4>1</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="card">
                <div class="card-header bg-info border-0">
                  <h4 class="card-title text-uppercase">
                    <i class="fas fa-user-secret mr-2"></i>Librarian
                  </h4>
                </div>
                <div class="card-body text-uppercase text-center">
                  <h4>1</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <div class="card-header bg-info border-0">
                  <h4 class="card-title text-uppercase">
                    <i class="fas fa-user-secret mr-2"></i>Receptionist
                  </h4>
                </div>
                <div class="card-body text-uppercase text-center">
                  <h4>1</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <div class="card-header bg-info border-0">
                  <h4 class="card-title text-uppercase">
                    <i class="fas fa-user-secret mr-2"></i>Super Admin
                  </h4>
                </div>
                <div class="card-body text-uppercase text-center">
                  <h4>1</h4>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header bg-success">
                  <div class="row text-center">
                    <div class="col-md-3">
                      <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-success mr-2"><i class="fas fa-chevron-left"></i></button>
                        <button type="button" class="btn btn-success"><i class="fas fa-chevron-right"></i></button>
                        <button type="button" class="btn btn-success">Today</button>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <h3>October 2021</h3>
                    </div>
                    <div class="col-md-3">
                      <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-success">Month</button>
                        <button type="button" class="btn btn-success">Week</button>
                        <button type="button" class="btn btn-success">Day</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table border table-hover">
                      <thead>
                        <tr>
                          <th scope="col">Sun</th>
                          <th scope="col">Mon</th>
                          <th scope="col">Tue</th>
                          <th scope="col">Web</th>
                          <th scope="col">Thu</th>
                          <th scope="col">Fri</th>
                          <th scope="col">Sat</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td>1</td>
                          <td>2</td>
                        </tr>
                        <tr></tr>
                        <tr>
                          <td>3</td>
                          <td>4</td>
                          <td>5</td>
                          <td>6</td>
                          <td>7</td>
                          <td>8</td>
                          <td>9</td>
                        </tr>
                        <tr>
                          <td>10</td>
                          <td>11</td>
                          <td>12</td>
                          <td>13</td>
                          <td>14</td>
                          <td>15</td>
                          <td>16</td>
                        </tr>
                        <tr>
                          <td>17</td>
                          <td>18</td>
                          <td>19</td>
                          <td>20</td>
                          <td>21</td>
                          <td>22</td>
                          <td>23</td>
                        </tr>
                        <tr>
                          <td>24</td>
                          <td>25</td>
                          <td>26</td>
                          <td>27</td>
                          <td>28</td>
                          <td>29</td>
                          <td>30</td>
                        </tr>
                        <tr>
                          <td>31</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- container fluid end -->
      </section>
    </div>
  </div>

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2021 <a href="https://zoyoecommerce.com">Zoyo E-commerce Pvt. Ltd.</a></strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b class="mr-1">Version</b>0.1
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="plugins/moment/moment.min.js"></script>
  <script src="plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
</body>

</html>