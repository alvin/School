<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Print Admit Card- Zoyo School</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

    <!-- calender css -->
    <link rel="stylesheet" href="calendar/dist/style.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- Preloader -->

        <!-- top navbar -->
        <?php include('topnav.php') ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?php include('sidebar.php') ?>
        <!-- main sidebar end -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">

            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="">
                                <form action="#">
                                    <div class="row">
                                        <div class="col-md-12 mb-2">
                                            <div class="border p-1 mb-1">
                                                <h4>Print Admit Card</h4>
                                            </div>
                                            <div class="border p-1">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="examgroup">Exam Group</label>
                                                            <select name="examgroup" class="form-control" id="examgroup">
                                                                <option value="">Select</option>
                                                                <option value=""></option>
                                                            </select>
                                                            <span class=""></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exam">Exam</label>
                                                            <select name="exam" class="form-control" id="exam">
                                                                <option value="">Select</option>
                                                                <option value=""></option>
                                                            </select>
                                                            <span class=""></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="session">Session</label>
                                                            <span class=""></span>
                                                            <select class="form-control" id="session">
                                                                <option selected value="">Select</option>
                                                                <option>2021-2022</option>
                                                                <option>2022-2023</option>
                                                                <option>2023-2024</option>
                                                                <option>2024-2025</option>
                                                                <option>2025-2026</option>
                                                                <option>2026-2027</option>
                                                                <option>2027-2028</option>
                                                                <option>2028-2029</option>
                                                                <option>2029-2030</option>
                                                                <option>2030-2031</option>
                                                                <option>2031-2032</option>
                                                                <option>2032-2033</option>
                                                                <option>2033-2034</option>
                                                                <option>2034-2035</option>
                                                                <option>2035-2036</option>
                                                                <option>2036-2037</option>
                                                                <option>2037-2038</option>
                                                                <option>2038-2039</option>
                                                                <option>2039-2040</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="ControlSelect1">Class</label>
                                                            <span class=""></span>
                                                            <select class="form-control" id="ControlSelect1">
                                                                <option selected>Select</option>
                                                                <option>Class 1</option>
                                                                <option>Class 2</option>
                                                                <option>Class 3</option>
                                                                <option>Class 4</option>
                                                                <option>Class 5</option>
                                                                <option>Class 6</option>
                                                                <option>Class 7</option>
                                                                <option>Class 8</option>
                                                                <option>Class 9</option>
                                                                <option>Class 10</option>
                                                                <option>Class 11</option>
                                                                <option>Class 12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="section">Section</label>
                                                            <span class=""></span>
                                                            <select class="form-control" id="section">
                                                                <option selected>Select</option>
                                                                <option>Section A</option>
                                                                <option>Section B</option>
                                                                <option>Section C</option>
                                                                <option>Section D</option>
                                                                <option>Section E</option>
                                                                <option>Section F</option>
                                                                <option>Section G</option>
                                                                <option>Section H</option>
                                                                <option>Section I</option>
                                                                <option>Section J</option>
                                                                <option>Section K</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="admitcardtemplate">Admit Card Template</label>
                                                            <select class="form-control" id="admitcardtemplate">
                                                                <option selected>Select</option>
                                                                <span class=""></span>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row text-right">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-secondary">Search</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <form action="">
                                    <!-- demo not completed -->
                                    <!-- uncomplete list -->
                                    <div class="col-md-12 mb-1">
                                        <div class="border p-1 mb-1">
                                            <h4>Admit Card List</h4>
                                        </div>
                                        <div class="row justify-content-between">
                                            <div class="col-md-4 mb-1">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search" title="Type in a name">
                                                </div>
                                            </div>
                                            <div class="col-md-4 text-center mb-1">
                                                <button type="submit" class="btn border"><i class="fas fa-copy"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-file-excel"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-file-csv"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-file-pdf"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-print"></i></button>
                                            </div>
                                        </div>
                                        <!-- table -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <form action="#">
                                                        <table id="myTable" class="table border table-hover">
                                                            <tr class="header">
                                                                <th scope="col">Certificate Name</th>
                                                                <th scope="col">Background Image</th>
                                                                <th scope="col">Action</th>
                                                            </tr>
                                                            <tr>
                                                                <td><a href="">Sample</a></td>
                                                                <td><img src="" alt="img_not_found"></td>
                                                                <td>
                                                                    <div class="" style="width: 120px;">
                                                                        <div class="row justify-content-right">
                                                                            <div class="col-md-4 col-4">
                                                                                <form action="#">
                                                                                    <button type="submit" class="btn border mb-1 mr-1"><span><i class="fas fa-pen"></i></span></button>
                                                                                </form>
                                                                            </div>
                                                                            <div class="col-md-4 col-4">
                                                                                <form action="#">
                                                                                    <button type="submit" class="btn border mb-1"><span><i class="fas fa-times"></i></span></button>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- table end -->
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- container fluid end -->
            </section>
        </div>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2021 <a href="https://zoyoecommerce.com">Zoyo E-commerce Pvt. Ltd.</a></strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b class="mr-1">Version</b>0.1
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- table search js -->
    <script src="/dist/js/tablescript.js"></script>
    <!-- end table search js -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="plugins/moment/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
</body>

</html>