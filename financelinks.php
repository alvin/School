<div class="row">
    <div class="col-md-12 mb-2 student-info-links">
        <div class="border px-3 py-1 mb-1">
            <h4>Finance Report</h4>
        </div>
        <div class="border p-3">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="feesstatement.php">Fees Statement</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="balancefeesreport.php">Balance Fees Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="feescollectionreport.php">Fees Collection Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="onlinefeescollectionreport.php">Online Fees Collection Report</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="incomereport.php">Income Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="expensereport.php">Expense Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="payrollreport.php">Payroll Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="incomegroupreport.php">Income Group Report</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="expensegroupreport.php">Expense Group Report</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>