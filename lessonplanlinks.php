<div class="row">
    <div class="col-md-12 mb-2 student-info-links">
        <div class="border px-3 py-1 mb-1">
            <h4>Lesson Plan Report</h4>
        </div>
        <div class="border p-3">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="syllabusstatusreport.php">Syllabus Status Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="subjectlessonplanreport.php">Subject Lesson Plan Report</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>