<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Staff Creation - Zoyo School</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

    <!-- calender css -->
    <link rel="stylesheet" href="calendar/dist/style.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- Preloader -->

        <!-- top navbar -->
        <?php include('topnav.php') ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?php include('sidebar.php') ?>
        <!-- main sidebar end -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header"></div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="">
                                <form action="">
                                    <!-- select criteria -->

                                    <div class="p-2 border">
                                        <div class="row mb-2">
                                            <div class="col-md-12">
                                                <h5>Staff Registration</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 mb-1">
                                                <label for="admissionno">Staff ID</label>
                                                <input type="number" class="form-control" id="admissionno">
                                            </div>
                                            <div class="col-md-3 mb-1">
                                                <div class="form-group">
                                                    <label for="role">Role</label>
                                                    <select class="form-control" id="role">
                                                        <option selected>Select</option>
                                                        <option>Teacher</option>
                                                        <option>Accountant</option>
                                                        <option>Librarian</option>
                                                        <option>Receptionist</option>
                                                        <option>Admin</option>
                                                        <option>Super Admin</option>
                                                    </select>
                                                    <span class=""></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-1">
                                                <div class="form-group">
                                                    <label for="disignation">Designation</label>
                                                    <select class="form-control" id="disignation">
                                                        <option selected>Select</option>
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3 mb-1">
                                                <div class="form-group">
                                                    <label for="department">Department</label>
                                                    <select class="form-control" id="department">
                                                        <option selected>Select</option>
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="firstname">First Name</label>
                                                    <input type="text" class="form-control" id="firstname">
                                                    <span class=""></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="lastname">Last Name</label>
                                                    <input type="text" class="form-control" id="lastname">
                                                    <span class=""></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="fathername">Fater Name</label>
                                                    <input type="text" class="form-control" id="fathername">
                                                    <span class=""></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="mothername">Mother Name</label>
                                                    <input type="text" class="form-control" id="mothername">
                                                    <span class=""></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="email">Email(Login Username)</label>
                                                    <input type="email" class="form-control" id="email">
                                                    <span class=""></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="gender">Gender</label>
                                                    <select class="form-control" id="gender">
                                                        <option selected>Select</option>
                                                        <option>Male</option>
                                                        <option>Female</option>
                                                        <option>Other</option>
                                                    </select>
                                                    <span class=""></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="dob">Date of Birth</label>
                                                    <input type="date" class="form-control" id="dob">
                                                    <span class=""></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="mobile">Mobile No</label>
                                                    <input type="number" class="form-control" id="mobile">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="doj">Date of Joining</label>
                                                    <input type="date" class="form-control" id="doj">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="emercontnum">Emergency Contact Number</label>
                                                    <input type="number" class="form-control" id="emercontnum">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="maritalstatus">Marital Status</label>
                                                    <select class="form-control" id="maritalstatus">
                                                        <option selected>Select</option>
                                                        <option>Single</option>
                                                        <option>Married</option>
                                                        <option>Widowed</option>
                                                        <option>Separated</option>
                                                        <option>Not Specified</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="bloodgroup">Blood Group</label>
                                                    <select class="form-control" id="bloodgroup">
                                                        <option selected>Select</option>
                                                        <option>O+</option>
                                                        <option>A+</option>
                                                        <option>B+</option>
                                                        <option>O-</option>
                                                        <option>A-</option>
                                                        <option>B-</option>
                                                        <option>AB-</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="photo">Photo</label>
                                                    <input type="file" class="form-control" id="photo">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="currentaddress">Current Address</label>
                                                    <textarea class="form-control" id="currentaddress"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="permanentaddress">Permanent Address</label>
                                                    <textarea class="form-control" id="permanentaddress"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="qualification">Qualification</label>
                                                    <textarea class="form-control" id="qualification"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="workexperience">Work Experience</label>
                                                    <textarea name="workexperience" id="workexperience" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="medicalhistory">Medical History</label>
                                                    <textarea name="medicalhistory" id="medicalhistory" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="note">Note</label>
                                                    <textarea name="note" id="note" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- add more detail -->
                                        <div class="mt-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="card">
                                                        <a class="" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                            <div class="card-header text-dark">
                                                                <h5> Add More Detail</h5>
                                                            </div>
                                                        </a>

                                                        <div class="collapse" id="collapseExample">
                                                            <div class="card-body">
                                                                <div class="studentaddressdetail mb-4">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="card-title">
                                                                                <h5>Payroll</h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mt-2">
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label for="epfno">EPF No</label>
                                                                                <input type="text" class="form-control" id="epfno">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label for="basicsalary">Basic Salary</label>
                                                                                <input type="text" class="form-control" id="basicsalary">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label for="workshift">Work Shift</label>
                                                                                <input type="text" class="form-control" id="workshift">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label for="contracttype">Contract Type</label>
                                                                                <select class="form-control" id="contracttype">
                                                                                    <option selected value="">Select</option>
                                                                                    <option>Permanent</option>
                                                                                    <option>Probation</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label for="location">Location</label>
                                                                                <input type="text" class="form-control" id="location">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <hr class="mt-0 mb-3">

                                                                <div class="miscellaneousdetails ">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="card-title">
                                                                                <h5>Bank Account Details </h5>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row mt-2">
                                                                        <div class="col-md-3 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="accounttitle">Account Title</label>
                                                                                <input type="text" class="form-control" id="accounttitle">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="bankaccno">Bank Account Number</label>
                                                                                <input type="text" class="form-control" id="bankaccno">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="bankname">Bank Name</label>
                                                                                <input type="text" class="form-control" id="bankname">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="ifsccode">IFSC Code</label>
                                                                                <input type="text" class="form-control" id="ifsccode">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-3 mb-1">
                                                                            <div class="form-group">
                                                                                <label for="branch">Bank Branch Name</label>
                                                                                <input type="text" class="form-control" id="branch">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <hr class="my-3">

                                                                    <div class="socialmedialinks">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="card-title">
                                                                                    <h5>Social Media Links</h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-3 mb-1">
                                                                                <div class="form-group">
                                                                                    <label for="facebook">Facebook URL</label>
                                                                                    <input type="text" class="form-control" id="facebook">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 mb-1">
                                                                                <div class="form-group">
                                                                                    <label for="instagram">Instagram URL</label>
                                                                                    <input type="text" class="form-control" id="instagram">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 mb-1">
                                                                                <div class="form-group">
                                                                                    <label for="twitter">Twitter URL</label>
                                                                                    <input type="text" class="form-control" id="twitter">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 mb-1">
                                                                                <div class="form-group">
                                                                                    <label for="linkedin">Linkedin URL</label>
                                                                                    <input type="text" class="form-control" id="linkedin">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <hr class="my-3">

                                                                    <div class="transportdetail">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="card-title">
                                                                                    <h5>Upload Documents</h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-3 mb-1">
                                                                                <div class="form-group">
                                                                                    <label for="file2">Resume</label>
                                                                                    <input type="file" class="form-control" id="file1">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 mb-1">
                                                                                <div class="form-group">
                                                                                    <label for="title2">Joining Letter</label>
                                                                                    <input type="file" class="form-control" id="title2">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 mb-1">
                                                                                <div class="form-group">
                                                                                    <label for="title1">Other Document</label>
                                                                                    <input type="file" class="form-control" id="title1">
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- /.row -->
                                        <div class="row justify-content-end mt-2">
                                            <div class="col-md-4 text-right">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-secondary">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- container fluid end -->
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2021 <a href="https://zoyoecommerce.com">Zoyo E-commerce Pvt. Ltd.</a></strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b class="mr-1">Version</b>0.1
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- table search js -->
    <script>
        function myFunction() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
    <!-- end table search js -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="plugins/moment/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
</body>

</html>