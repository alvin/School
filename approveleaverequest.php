<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Approve Leave Request - Zoyo School</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

    <!-- calender css -->
    <link rel="stylesheet" href="calendar/dist/style.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- Preloader -->

        <!-- top navbar -->
        <?php include('topnav.php') ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?php include('sidebar.php') ?>
        <!-- main sidebar end -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">

            </div>
            <!-- /.content-header -->

            <section class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header py-1">
                            <div class="row justify-content-between">
                                <div class="col-md-4 col-6 text-left">
                                    <h4>Approve Leave Request</h4>
                                </div>
                                <div class="col-md-4 col-6 text-right">
                                    <button class="btn btn-secondary" data-target="#myModal" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                                        Add Leave Request
                                    </button>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <form action="">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="Role">Role</label>
                                                            <input type="text" class="form-control" id="role">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="name">Name</label>
                                                            <input type="text" class="form-control" id="name">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="applydate">Apply Date</label>
                                                            <input type="date" class="form-control" id="applydate">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="leavetype">Leave Type</label>
                                                            <select name="leavetype" id="leavetype" class="form-control">
                                                                <option value="">Select</option>
                                                                <option value=""></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="leavefromdate">Leave From Date</label>
                                                            <input type="date" class="form-control" id="leavefromdate">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="leavetodate">Leave To Date</label>
                                                            <input type="date" class="form-control" id="leavetodate">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="reason">Reason</label>
                                                            <textarea name="reason" id="reason" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="note">Note</label>
                                                            <textarea name="note" id="note" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="doc">Attach Document</label>
                                                            <input type="file" class="form-control" id="doc">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="status">Status</label>
                                                            <select name="status" id="status" class="form-control">
                                                                <option value="">Select</option>
                                                                <option value="pending">Pending</option>
                                                                <option value="approve">Approve</option>
                                                                <option value="disapprove">Disapprove</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <div class="card-body">
                            <div class="">
                                <form action="#">
                                    <div class="row">
                                        <div class="col-md-12 mb-2">
                                            <div class="border p-1 mb-1">
                                                <h4>Select Criteria</h4>
                                            </div>
                                            <div class="border p-2">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="role">Role</label>
                                                            <select class="form-control" id="role">
                                                                <option selected>Select</option>
                                                                <option>Teacher</option>
                                                                <option>Accountant</option>
                                                                <option>Librarian</option>
                                                                <option>Receptionist</option>
                                                                <option>Admin</option>
                                                                <option>Super Admin</option>
                                                            </select>
                                                            <span class=""></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="month">Month</label>
                                                            <select name="month" id="month" class="form-control">
                                                                <option value="">Select</option>
                                                                <option value="january">January</option>
                                                                <option value="february">February</option>
                                                                <option value="march">march</option>
                                                                <option value="april">April</option>
                                                                <option value="may">May</option>
                                                                <option value="june">June</option>
                                                                <option value="july">July</option>
                                                                <option value="auguest">Auguest</option>
                                                                <option value="september">September</option>
                                                                <option value="october">October</option>
                                                                <option value="november">November</option>
                                                                <option value="december">December</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row text-right">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-secondary"> Search</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <div class="row">
                                    <!-- demo not completed -->
                                    <!-- uncomplete list -->
                                    <div class="col-md-12 mb-1">
                                        <div class="border p-1 mb-1">
                                            <h4>Approve Leave Request List</h4>
                                        </div>
                                        <div class="row justify-content-between">
                                            <div class="col-md-4 mb-1">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search" title="Type in a name">
                                                </div>
                                            </div>
                                            <div class="col-md-4 text-center mb-1">
                                                <button type="submit" class="btn border"><i class="fas fa-copy"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-file-excel"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-file-csv"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-file-pdf"></i></button>
                                                <button type="submit" class="btn border"><i class="fas fa-print"></i></button>
                                            </div>
                                        </div>
                                        <!-- table -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <form action="#">
                                                        <table id="myTable" class="table border table-hover">
                                                            <tr class="header">
                                                                <th scope="col">Staff</th>
                                                                <th scope="col">Leave Type</th>
                                                                <th scope="col">Leave Date</th>
                                                                <th scope="col">Days</th>
                                                                <th scope="col">Apply Date</th>
                                                                <th scope="col">Status</th>
                                                                <th scope="col">Action</th>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <div class="" style="width: 120px;">
                                                                        <div class="row justify-content-right">
                                                                            <div class="col-md-4 col-4">
                                                                                <form action="#">
                                                                                    <button type="submit" class="btn border mb-1 mr-1"><span><i class="fas fa-pen"></i></span></button>
                                                                                </form>
                                                                            </div>
                                                                            <div class="col-md-4 col-4">
                                                                                <form action="#">
                                                                                    <button type="submit" class="btn border mb-1"><span><i class="fas fa-times"></i></span></button>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- table end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- container fluid end -->
            </section>
        </div>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2021 <a href="https://zoyoecommerce.com">Zoyo E-commerce Pvt. Ltd.</a></strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b class="mr-1">Version</b>0.1
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- table search js -->
    <script src="/dist/js/tablescript.js"></script>
    <!-- end table search js -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="plugins/moment/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
</body>

</html>