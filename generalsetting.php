<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>General Setting - Zoyo School</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
    <!-- calender css -->
    <link rel="stylesheet" href="calendar/dist/style.css">
    <link rel="stylesheet" href="dist/css/style.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- Preloader -->

        <!-- top navbar -->
        <?php include('topnav.php') ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?php include('sidebar.php') ?>
        <!-- main sidebar end -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">

            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="">
                                <form action="">
                                    <div class="row">
                                        <div class="col-md-3 mb-3">
                                            <div class="card">
                                                <div class="card-body text-center">
                                                    <img src="" class="card-img" alt="img-not-found">
                                                    <a href="" class="btn btn-secondary mt-2">Edit Print Logo</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <div class="card">
                                                <div class="card-body text-center">
                                                    <img src="" class="card-img" alt="img-not-found">
                                                    <a href="" class="btn btn-secondary mt-2">Edit Admin Logo</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <div class="card">
                                                <div class="card-body text-center">
                                                    <img src="" class="card-img" alt="img-not-found">
                                                    <a href="" class="btn btn-secondary mt-2">Edit Admin Small Logo</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <div class="card">
                                                <div class="card-body text-center">
                                                    <img src="" class="card-img" alt="img-not-found">
                                                    <a href="" class="btn btn-secondary mt-2">Edit App Logo</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="border mb-2">
                                        <h4 class="mt-1">General Setting</h4>
                                    </div>
                                    <form action="">
                                        <div class="sec">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="schoolname">School Name</label>
                                                        <input type="text" class="form-control" id="schoolname" value="">
                                                        <span class=""></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="schoolcode">School Code</label>
                                                        <input type="text" class="form-control" id="schoolcode" value="">
                                                        <span class=""></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="phone">Phone</label>
                                                        <input type="text" class="form-control" id="phone" value="">
                                                        <span class=""></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="email">School Name</label>
                                                        <input type="text" class="form-control" id="email" value="">
                                                        <span class=""></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="address">Address</label>
                                                        <input type="text" class="form-control" id="address" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="sec">
                                            <div class="row mb-2">
                                                <div class="col-md-12">
                                                    <h4>Session</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="session">Session</label>
                                                        <select name="session" id="session" class="form-control">
                                                            <option value="" selected>Select</option>
                                                            <option>2020-2021</option>
                                                            <option>2021-2022</option>
                                                            <option>2022-2023</option>
                                                            <option>2023-2024</option>
                                                            <option>2024-2025</option>
                                                            <option>2025-2026</option>
                                                            <option>2026-2027</option>
                                                            <option>2027-2028</option>
                                                            <option>2028-2029</option>
                                                            <option>2029-2030</option>
                                                        </select>
                                                        <span class=""></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="sessionstartmonth">Session Start Month</label>
                                                        <select name="sessionstartmonth" id="sessionstartmonth" class="form-control">
                                                            <option value="" selected>Select</option>
                                                            <option>January</option>
                                                            <option>February</option>
                                                            <option>March</option>
                                                            <option>April</option>
                                                            <option>May</option>
                                                            <option>June</option>
                                                            <option>July</option>
                                                            <option>August</option>
                                                            <option>September</option>
                                                            <option>October</option>
                                                        </select>
                                                        <span class=""></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="sec">
                                            <div class="row mb-2">
                                                <div class="col-md-12">
                                                    <h4>Attendance Type</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="wise">Attendance</label>
                                                        <div class="form-check">
                                                            <input type="radio" class="form-check-input" id="wise" name="materialExampleRadios">
                                                            <label class="form-check-label" for="wise">Day Wise</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input type="radio" class="form-check-input" id="wise" name="materialExampleRadios" checked>
                                                            <label class="form-check-label" for="wise">Period Wise</label>
                                                        </div>
                                                        <span class=""></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="biometric">Biometric Attendance</label>
                                                        <div class="form-check">
                                                            <input type="radio" class="form-check-input" id="biometric" name="materialExampleRadios">
                                                            <label class="form-check-label" for="biometric">Day biometric</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input type="radio" class="form-check-input" id="biometric" name="materialExampleRadios" checked>
                                                            <label class="form-check-label" for="biometric">Period Wise</label>
                                                        </div>
                                                        <span class=""></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="devices">Devices (Separate By Coma)</label>
                                                        <input type="text" class="form-control" id="devices">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2021 <a href="https://zoyoecommerce.com">Zoyo E-commerce Pvt. Ltd.</a></strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b class="mr-1">Version</b>0.1
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <script type="text/javascript">
        $(document).ready(function() {
            $(studentInfo).click(function() {
                $(demo).show();

            });
        });
    </script>
    }

    <!-- table search js -->
    <script src="/dist/js/tablescript.js"></script>
    <!-- end table search js -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="plugins/moment/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
</body>

</html>