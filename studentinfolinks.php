<div class="row">
    <div class="col-md-12 mb-2 student-info-links">
        <div class="border px-3 py-1 mb-1">
            <h4>Student Information Report</h4>
        </div>
        <div class="border p-3">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="studentreport.php">Student Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="guardianreport.php">Guardian Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="studenthistory.php">Student History</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="studentlogincredential.php">Student Login Credential</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="">Class Subject Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="">Admission Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="">Sibling Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="">Student Profile</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="">Homework Evaluation Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="">Student Gender Ratio Report</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <p>
                            <i class="fas fa-file-alt"></i>
                            <a href="">Student Teacher Ratio Report</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>